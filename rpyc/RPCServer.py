import rpyc
from rpyc.utils.server import ThreadedServer
from multiprocessing import Lock
import time

size = 10
threadsInQueue = 0
wait = True
lock = Lock()

class RPCServer(rpyc.Service):
    def exposed_P(self, val):
        global size
        global lock
        global wait
        global threadsInQueue
        notify = False
        while val > size and not notify:
            threadsInQueue += 1
            while wait:
                placeholder = True
            with lock:
                threadsInQueue -= 1
                if threadsInQueue == 0:
                    wait = True
            while not wait:
                placeholder = True
            with lock:
                if size >= val:
                    size -= val
                    notify = True
        if not notify:
            size -= val
        print("Actual size after enter: " + str(size))

    def exposed_V(self, val):
        global size
        global wait
        global lock
        with lock:
            size += val
            wait = False
        print("Actual size after Out is " + str(size))


if __name__ == "__main__":
    server = ThreadedServer(RPCServer, port = 18812)
    server.start()