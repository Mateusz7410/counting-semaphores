import rpyc
import time
from random import randrange

class RPCClient:
    def __init__(self):
        self.semaphore = rpyc.connect("localhost", 18812, config={'sync_request_timeout': None})       

    def lock(self, val):
        return self.semaphore.root.P(val)
    
    def unlock(self, val):
        return self.semaphore.root.V(val)

if __name__ == "__main__":
    client = RPCClient()
    while True:
        val = randrange(10) + 1
        print("I am trying to take " + str(val) + " unit/s from semaphore!")
        client.lock(val)
        print("Enter the CS.")
        time.sleep(8)
        print("I am trying to return " + str(val) + " unit/s from semaphore!")
        client.unlock(val)
        print("Left the CS")